﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmQ8
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.imgQ8_MainImage = New System.Windows.Forms.PictureBox()
        Me.btnQ8_OK = New System.Windows.Forms.Button()
        Me.rbtnQ8_Choice0 = New System.Windows.Forms.RadioButton()
        Me.rbtnQ8_Choice3 = New System.Windows.Forms.RadioButton()
        Me.rbtnQ8_Choice2 = New System.Windows.Forms.RadioButton()
        Me.rbtnQ8_Choice1 = New System.Windows.Forms.RadioButton()
        Me.lblQ8_Choice0 = New System.Windows.Forms.Label()
        Me.lblQ8_Choice1 = New System.Windows.Forms.Label()
        Me.lblQ8_Choice3 = New System.Windows.Forms.Label()
        Me.lblQ8_Choice2 = New System.Windows.Forms.Label()
        Me.btnQ8_Cancel = New System.Windows.Forms.Button()
        Me.lblQ8_MainQuestion = New System.Windows.Forms.Label()
        CType(Me.imgQ8_MainImage, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'imgQ8_MainImage
        '
        Me.imgQ8_MainImage.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom
        Me.imgQ8_MainImage.Location = New System.Drawing.Point(12, 12)
        Me.imgQ8_MainImage.Name = "imgQ8_MainImage"
        Me.imgQ8_MainImage.Size = New System.Drawing.Size(240, 180)
        Me.imgQ8_MainImage.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.imgQ8_MainImage.TabIndex = 0
        Me.imgQ8_MainImage.TabStop = False
        '
        'btnQ8_OK
        '
        Me.btnQ8_OK.Location = New System.Drawing.Point(192, 335)
        Me.btnQ8_OK.Name = "btnQ8_OK"
        Me.btnQ8_OK.Size = New System.Drawing.Size(60, 25)
        Me.btnQ8_OK.TabIndex = 1
        Me.btnQ8_OK.Text = "OK"
        Me.btnQ8_OK.UseVisualStyleBackColor = True
        '
        'rbtnQ8_Choice0
        '
        Me.rbtnQ8_Choice0.AutoSize = True
        Me.rbtnQ8_Choice0.Font = New System.Drawing.Font("Lucida Sans Typewriter", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.rbtnQ8_Choice0.ForeColor = System.Drawing.Color.RoyalBlue
        Me.rbtnQ8_Choice0.Location = New System.Drawing.Point(32, 219)
        Me.rbtnQ8_Choice0.Name = "rbtnQ8_Choice0"
        Me.rbtnQ8_Choice0.Size = New System.Drawing.Size(36, 22)
        Me.rbtnQ8_Choice0.TabIndex = 2
        Me.rbtnQ8_Choice0.TabStop = True
        Me.rbtnQ8_Choice0.Text = "A"
        Me.rbtnQ8_Choice0.UseVisualStyleBackColor = True
        '
        'rbtnQ8_Choice3
        '
        Me.rbtnQ8_Choice3.AccessibleDescription = ""
        Me.rbtnQ8_Choice3.AutoSize = True
        Me.rbtnQ8_Choice3.Font = New System.Drawing.Font("Lucida Sans Typewriter", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.rbtnQ8_Choice3.ForeColor = System.Drawing.Color.RoyalBlue
        Me.rbtnQ8_Choice3.Location = New System.Drawing.Point(32, 303)
        Me.rbtnQ8_Choice3.Name = "rbtnQ8_Choice3"
        Me.rbtnQ8_Choice3.Size = New System.Drawing.Size(36, 22)
        Me.rbtnQ8_Choice3.TabIndex = 3
        Me.rbtnQ8_Choice3.TabStop = True
        Me.rbtnQ8_Choice3.Text = "D"
        Me.rbtnQ8_Choice3.UseVisualStyleBackColor = True
        '
        'rbtnQ8_Choice2
        '
        Me.rbtnQ8_Choice2.AutoSize = True
        Me.rbtnQ8_Choice2.Font = New System.Drawing.Font("Lucida Sans Typewriter", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.rbtnQ8_Choice2.ForeColor = System.Drawing.Color.RoyalBlue
        Me.rbtnQ8_Choice2.Location = New System.Drawing.Point(32, 275)
        Me.rbtnQ8_Choice2.Name = "rbtnQ8_Choice2"
        Me.rbtnQ8_Choice2.Size = New System.Drawing.Size(36, 22)
        Me.rbtnQ8_Choice2.TabIndex = 4
        Me.rbtnQ8_Choice2.TabStop = True
        Me.rbtnQ8_Choice2.Text = "C"
        Me.rbtnQ8_Choice2.UseVisualStyleBackColor = True
        '
        'rbtnQ8_Choice1
        '
        Me.rbtnQ8_Choice1.AutoSize = True
        Me.rbtnQ8_Choice1.Font = New System.Drawing.Font("Lucida Sans Typewriter", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.rbtnQ8_Choice1.ForeColor = System.Drawing.Color.RoyalBlue
        Me.rbtnQ8_Choice1.Location = New System.Drawing.Point(32, 247)
        Me.rbtnQ8_Choice1.Name = "rbtnQ8_Choice1"
        Me.rbtnQ8_Choice1.Size = New System.Drawing.Size(36, 22)
        Me.rbtnQ8_Choice1.TabIndex = 5
        Me.rbtnQ8_Choice1.TabStop = True
        Me.rbtnQ8_Choice1.Text = "B"
        Me.rbtnQ8_Choice1.UseVisualStyleBackColor = True
        '
        'lblQ8_Choice0
        '
        Me.lblQ8_Choice0.AutoSize = True
        Me.lblQ8_Choice0.Location = New System.Drawing.Point(91, 224)
        Me.lblQ8_Choice0.Name = "lblQ8_Choice0"
        Me.lblQ8_Choice0.Size = New System.Drawing.Size(64, 13)
        Me.lblQ8_Choice0.TabIndex = 6
        Me.lblQ8_Choice0.Text = "<country A>"
        '
        'lblQ8_Choice1
        '
        Me.lblQ8_Choice1.AutoSize = True
        Me.lblQ8_Choice1.Location = New System.Drawing.Point(90, 252)
        Me.lblQ8_Choice1.Name = "lblQ8_Choice1"
        Me.lblQ8_Choice1.Size = New System.Drawing.Size(64, 13)
        Me.lblQ8_Choice1.TabIndex = 7
        Me.lblQ8_Choice1.Text = "<country B>"
        '
        'lblQ8_Choice3
        '
        Me.lblQ8_Choice3.AutoSize = True
        Me.lblQ8_Choice3.Location = New System.Drawing.Point(90, 308)
        Me.lblQ8_Choice3.Name = "lblQ8_Choice3"
        Me.lblQ8_Choice3.Size = New System.Drawing.Size(65, 13)
        Me.lblQ8_Choice3.TabIndex = 8
        Me.lblQ8_Choice3.Text = "<country D>"
        '
        'lblQ8_Choice2
        '
        Me.lblQ8_Choice2.AutoSize = True
        Me.lblQ8_Choice2.Location = New System.Drawing.Point(90, 279)
        Me.lblQ8_Choice2.Name = "lblQ8_Choice2"
        Me.lblQ8_Choice2.Size = New System.Drawing.Size(64, 13)
        Me.lblQ8_Choice2.TabIndex = 9
        Me.lblQ8_Choice2.Text = "<country C>"
        '
        'btnQ8_Cancel
        '
        Me.btnQ8_Cancel.Location = New System.Drawing.Point(126, 335)
        Me.btnQ8_Cancel.Name = "btnQ8_Cancel"
        Me.btnQ8_Cancel.Size = New System.Drawing.Size(60, 25)
        Me.btnQ8_Cancel.TabIndex = 10
        Me.btnQ8_Cancel.Text = "Cancel"
        Me.btnQ8_Cancel.UseVisualStyleBackColor = True
        '
        'lblQ8_MainQuestion
        '
        Me.lblQ8_MainQuestion.AutoSize = True
        Me.lblQ8_MainQuestion.Font = New System.Drawing.Font("Lucida Sans", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblQ8_MainQuestion.Location = New System.Drawing.Point(17, 198)
        Me.lblQ8_MainQuestion.Name = "lblQ8_MainQuestion"
        Me.lblQ8_MainQuestion.Size = New System.Drawing.Size(229, 15)
        Me.lblQ8_MainQuestion.TabIndex = 11
        Me.lblQ8_MainQuestion.Text = "Which country has this kind of flag?"
        '
        'frmQ8
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(264, 372)
        Me.ControlBox = False
        Me.Controls.Add(Me.lblQ8_MainQuestion)
        Me.Controls.Add(Me.btnQ8_Cancel)
        Me.Controls.Add(Me.lblQ8_Choice2)
        Me.Controls.Add(Me.lblQ8_Choice3)
        Me.Controls.Add(Me.lblQ8_Choice1)
        Me.Controls.Add(Me.lblQ8_Choice0)
        Me.Controls.Add(Me.rbtnQ8_Choice1)
        Me.Controls.Add(Me.rbtnQ8_Choice2)
        Me.Controls.Add(Me.rbtnQ8_Choice3)
        Me.Controls.Add(Me.rbtnQ8_Choice0)
        Me.Controls.Add(Me.btnQ8_OK)
        Me.Controls.Add(Me.imgQ8_MainImage)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.Name = "frmQ8"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Question 9"
        CType(Me.imgQ8_MainImage, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents imgQ8_MainImage As System.Windows.Forms.PictureBox
    Friend WithEvents btnQ8_OK As System.Windows.Forms.Button
    Friend WithEvents rbtnQ8_Choice0 As System.Windows.Forms.RadioButton
    Friend WithEvents rbtnQ8_Choice3 As System.Windows.Forms.RadioButton
    Friend WithEvents rbtnQ8_Choice2 As System.Windows.Forms.RadioButton
    Friend WithEvents rbtnQ8_Choice1 As System.Windows.Forms.RadioButton
    Friend WithEvents lblQ8_Choice0 As System.Windows.Forms.Label
    Friend WithEvents lblQ8_Choice1 As System.Windows.Forms.Label
    Friend WithEvents lblQ8_Choice3 As System.Windows.Forms.Label
    Friend WithEvents lblQ8_Choice2 As System.Windows.Forms.Label
    Friend WithEvents btnQ8_Cancel As System.Windows.Forms.Button
    Friend WithEvents lblQ8_MainQuestion As System.Windows.Forms.Label
End Class
