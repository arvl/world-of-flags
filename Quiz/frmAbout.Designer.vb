﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmAbout
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.lblAbout_World = New System.Windows.Forms.Label()
        Me.lblAbout_Of = New System.Windows.Forms.Label()
        Me.lblAbout_Flags = New System.Windows.Forms.Label()
        Me.lblAbout_DescLine1 = New System.Windows.Forms.Label()
        Me.lblAbout_DescLine2 = New System.Windows.Forms.Label()
        Me.lblAbout_Copyright = New System.Windows.Forms.Label()
        Me.btnAbout_License = New System.Windows.Forms.Button()
        Me.btnAbout_Close = New System.Windows.Forms.Button()
        Me.SuspendLayout()
        '
        'lblAbout_World
        '
        Me.lblAbout_World.AutoSize = True
        Me.lblAbout_World.Font = New System.Drawing.Font("Lucida Sans", 24.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblAbout_World.ForeColor = System.Drawing.Color.SteelBlue
        Me.lblAbout_World.Location = New System.Drawing.Point(33, 9)
        Me.lblAbout_World.Name = "lblAbout_World"
        Me.lblAbout_World.Size = New System.Drawing.Size(113, 37)
        Me.lblAbout_World.TabIndex = 0
        Me.lblAbout_World.Text = "World"
        '
        'lblAbout_Of
        '
        Me.lblAbout_Of.AutoSize = True
        Me.lblAbout_Of.Font = New System.Drawing.Font("Lucida Sans", 20.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblAbout_Of.ForeColor = System.Drawing.Color.SteelBlue
        Me.lblAbout_Of.Location = New System.Drawing.Point(134, 22)
        Me.lblAbout_Of.Name = "lblAbout_Of"
        Me.lblAbout_Of.Size = New System.Drawing.Size(43, 31)
        Me.lblAbout_Of.TabIndex = 1
        Me.lblAbout_Of.Text = "of"
        '
        'lblAbout_Flags
        '
        Me.lblAbout_Flags.AutoSize = True
        Me.lblAbout_Flags.Font = New System.Drawing.Font("Lucida Sans", 39.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblAbout_Flags.ForeColor = System.Drawing.Color.SteelBlue
        Me.lblAbout_Flags.Location = New System.Drawing.Point(19, 38)
        Me.lblAbout_Flags.Name = "lblAbout_Flags"
        Me.lblAbout_Flags.Size = New System.Drawing.Size(168, 60)
        Me.lblAbout_Flags.TabIndex = 2
        Me.lblAbout_Flags.Text = "Flags"
        '
        'lblAbout_DescLine1
        '
        Me.lblAbout_DescLine1.AutoSize = True
        Me.lblAbout_DescLine1.Font = New System.Drawing.Font("Arial Narrow", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblAbout_DescLine1.Location = New System.Drawing.Point(23, 100)
        Me.lblAbout_DescLine1.Name = "lblAbout_DescLine1"
        Me.lblAbout_DescLine1.Size = New System.Drawing.Size(158, 16)
        Me.lblAbout_DescLine1.TabIndex = 3
        Me.lblAbout_DescLine1.Text = "VB.Net quiz application featuring"
        '
        'lblAbout_DescLine2
        '
        Me.lblAbout_DescLine2.AutoSize = True
        Me.lblAbout_DescLine2.Font = New System.Drawing.Font("Arial Narrow", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblAbout_DescLine2.Location = New System.Drawing.Point(23, 116)
        Me.lblAbout_DescLine2.Name = "lblAbout_DescLine2"
        Me.lblAbout_DescLine2.Size = New System.Drawing.Size(158, 16)
        Me.lblAbout_DescLine2.TabIndex = 4
        Me.lblAbout_DescLine2.Text = "randomized question generation"
        '
        'lblAbout_Copyright
        '
        Me.lblAbout_Copyright.AutoSize = True
        Me.lblAbout_Copyright.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblAbout_Copyright.Location = New System.Drawing.Point(10, 136)
        Me.lblAbout_Copyright.Name = "lblAbout_Copyright"
        Me.lblAbout_Copyright.Size = New System.Drawing.Size(183, 14)
        Me.lblAbout_Copyright.TabIndex = 5
        Me.lblAbout_Copyright.Text = "Copyright © 2018 The Lemon Project"
        '
        'btnAbout_License
        '
        Me.btnAbout_License.Location = New System.Drawing.Point(12, 157)
        Me.btnAbout_License.Name = "btnAbout_License"
        Me.btnAbout_License.Size = New System.Drawing.Size(75, 23)
        Me.btnAbout_License.TabIndex = 6
        Me.btnAbout_License.Text = "License"
        Me.btnAbout_License.UseVisualStyleBackColor = True
        '
        'btnAbout_Close
        '
        Me.btnAbout_Close.Location = New System.Drawing.Point(118, 157)
        Me.btnAbout_Close.Name = "btnAbout_Close"
        Me.btnAbout_Close.Size = New System.Drawing.Size(75, 23)
        Me.btnAbout_Close.TabIndex = 7
        Me.btnAbout_Close.Text = "Close"
        Me.btnAbout_Close.UseVisualStyleBackColor = True
        '
        'frmAbout
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(204, 192)
        Me.ControlBox = False
        Me.Controls.Add(Me.btnAbout_Close)
        Me.Controls.Add(Me.btnAbout_License)
        Me.Controls.Add(Me.lblAbout_Of)
        Me.Controls.Add(Me.lblAbout_World)
        Me.Controls.Add(Me.lblAbout_Copyright)
        Me.Controls.Add(Me.lblAbout_DescLine2)
        Me.Controls.Add(Me.lblAbout_DescLine1)
        Me.Controls.Add(Me.lblAbout_Flags)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.Name = "frmAbout"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "About"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents lblAbout_World As System.Windows.Forms.Label
    Friend WithEvents lblAbout_Of As System.Windows.Forms.Label
    Friend WithEvents lblAbout_Flags As System.Windows.Forms.Label
    Friend WithEvents lblAbout_DescLine1 As System.Windows.Forms.Label
    Friend WithEvents lblAbout_DescLine2 As System.Windows.Forms.Label
    Friend WithEvents lblAbout_Copyright As System.Windows.Forms.Label
    Friend WithEvents btnAbout_License As System.Windows.Forms.Button
    Friend WithEvents btnAbout_Close As System.Windows.Forms.Button
End Class
