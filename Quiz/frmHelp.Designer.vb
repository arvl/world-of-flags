﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmHelp
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmHelp))
        Me.lblHelp_Overview = New System.Windows.Forms.Label()
        Me.rtxHelp_OverviewMainText = New System.Windows.Forms.RichTextBox()
        Me.lblHelp_HowTo = New System.Windows.Forms.Label()
        Me.rtxHelp_HowToMainText = New System.Windows.Forms.RichTextBox()
        Me.btnAbout_Back = New System.Windows.Forms.Button()
        Me.SuspendLayout()
        '
        'lblHelp_Overview
        '
        Me.lblHelp_Overview.AutoSize = True
        Me.lblHelp_Overview.Font = New System.Drawing.Font("Lucida Sans", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblHelp_Overview.Location = New System.Drawing.Point(10, 9)
        Me.lblHelp_Overview.Name = "lblHelp_Overview"
        Me.lblHelp_Overview.Size = New System.Drawing.Size(63, 14)
        Me.lblHelp_Overview.TabIndex = 0
        Me.lblHelp_Overview.Text = "Overview:"
        '
        'rtxHelp_OverviewMainText
        '
        Me.rtxHelp_OverviewMainText.BackColor = System.Drawing.SystemColors.Control
        Me.rtxHelp_OverviewMainText.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.rtxHelp_OverviewMainText.Location = New System.Drawing.Point(12, 25)
        Me.rtxHelp_OverviewMainText.Name = "rtxHelp_OverviewMainText"
        Me.rtxHelp_OverviewMainText.ReadOnly = True
        Me.rtxHelp_OverviewMainText.Size = New System.Drawing.Size(260, 212)
        Me.rtxHelp_OverviewMainText.TabIndex = 1
        Me.rtxHelp_OverviewMainText.Text = resources.GetString("rtxHelp_OverviewMainText.Text")
        '
        'lblHelp_HowTo
        '
        Me.lblHelp_HowTo.AutoSize = True
        Me.lblHelp_HowTo.Font = New System.Drawing.Font("Lucida Sans", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblHelp_HowTo.Location = New System.Drawing.Point(10, 240)
        Me.lblHelp_HowTo.Name = "lblHelp_HowTo"
        Me.lblHelp_HowTo.Size = New System.Drawing.Size(51, 14)
        Me.lblHelp_HowTo.TabIndex = 2
        Me.lblHelp_HowTo.Text = "How To:"
        '
        'rtxHelp_HowToMainText
        '
        Me.rtxHelp_HowToMainText.BackColor = System.Drawing.SystemColors.Control
        Me.rtxHelp_HowToMainText.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.rtxHelp_HowToMainText.Location = New System.Drawing.Point(12, 256)
        Me.rtxHelp_HowToMainText.Name = "rtxHelp_HowToMainText"
        Me.rtxHelp_HowToMainText.ReadOnly = True
        Me.rtxHelp_HowToMainText.Size = New System.Drawing.Size(260, 159)
        Me.rtxHelp_HowToMainText.TabIndex = 3
        Me.rtxHelp_HowToMainText.Text = resources.GetString("rtxHelp_HowToMainText.Text")
        '
        'btnAbout_Back
        '
        Me.btnAbout_Back.Location = New System.Drawing.Point(197, 400)
        Me.btnAbout_Back.Name = "btnAbout_Back"
        Me.btnAbout_Back.Size = New System.Drawing.Size(75, 23)
        Me.btnAbout_Back.TabIndex = 4
        Me.btnAbout_Back.Text = "Back"
        Me.btnAbout_Back.UseVisualStyleBackColor = True
        '
        'frmHelp
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(284, 431)
        Me.ControlBox = False
        Me.Controls.Add(Me.btnAbout_Back)
        Me.Controls.Add(Me.rtxHelp_HowToMainText)
        Me.Controls.Add(Me.lblHelp_HowTo)
        Me.Controls.Add(Me.rtxHelp_OverviewMainText)
        Me.Controls.Add(Me.lblHelp_Overview)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.Name = "frmHelp"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Help"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents lblHelp_Overview As System.Windows.Forms.Label
    Friend WithEvents rtxHelp_OverviewMainText As System.Windows.Forms.RichTextBox
    Friend WithEvents lblHelp_HowTo As System.Windows.Forms.Label
    Friend WithEvents rtxHelp_HowToMainText As System.Windows.Forms.RichTextBox
    Friend WithEvents btnAbout_Back As System.Windows.Forms.Button
End Class
