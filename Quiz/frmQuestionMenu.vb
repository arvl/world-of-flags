﻿Public Class frmQuestionMenu

    Public randomNumberGenerator As New Random()
    Public quizImagesPath As String = "C:\assets\"
    Public quizImagesExt As String = ".png"
    Public quizImages() As String = {"Afghanistan", "Albania", "Algeria", "Andorra", "Angola", "Antigua and Barbuda", "Argentina", "Armenia", "Australia", "Austria", "Azerbaijan", "Bahamas", "Bahrain", "Bangladesh", "Barbados", "Belarus", "Belgium", "Belize", "Benin", "Bhutan", "Bolivia", "Bosnia and Herzegovina", "Botswana", "Brazil", "Brunei", "Bulgaria", "Burkina Faso", "Burundi", "Cambodia", "Cameroon", "Canada", "Cape Verde", "Central African Republic", "Chad", "Chile", "China", "Colombia", "Comoros", "Costa Rica", "Côte d'Ivoire", "Croatia", "Cuba", "Cyprus", "Czech Republic", "Democratic Republic of Congo", "Denmark", "Djibouti", "Dominican Republic", "Dominica", "Ecuador", "Egypt", "El Salvador", "Equatorial Guinea", "Eritrea", "Estonia", "Ethopia", "Fiji", "Finland", "France", "Gabon", "Gambia", "Georgia", "Germany", "Ghana", "Greece", "Grenada", "Guatemala", "Guinea-Bissau", "Guinea", "Guyana", "Haiti", "Honduras", "Hungary", "Iceland", "India", "Indonesia", "Iran", "Iraq", "Ireland", "Israel", "Italy", "Jamaica", "Japan", "Jordan", "Kazakhstan", "Kenya", "Kiribati", "Kosovo", "Kuwait", "Kyrgyzstan", "Laos", "Latvia", "Lebanon", "Lesotho", "Liberia", "Libya", "Liechtenstein", "Lithuania", "Luxembourg", "Macedonia", "Madagascar", "Mauritania", "Malawi", "Malaysia", "Maldives", "Mali", "Malta", "Marshal Islands", "Mauritius", "Mexico", "Micronesia", "Moldova", "Monaco", "Mongolia", "Montenegro", "Morocco", "Mozambique", "Myanmar", "Namibia", "Nauru", "Nepal", "Netherlands", "New Zealand", "Nicaragua", "Nigeria", "Niger", "North Korea", "Norway", "Oman", "Pakistan", "Palau", "Panama", "Papua New Guinea", "Paraguay", "Peru", "Philippines", "Poland", "Portugal", "Qatar", "Republic of Congo", "Romania", "Russia", "Rwanda", "Saint Kitts and Nevis", "Saint Lucia", "Saint Vincent and Grenadines", "San Marino", "Sao Tome and Principe", "Saudia Arabia", "Senegal", "Serbia", "Seychelles", "Sierra Leone", "Singapore", "Slovakia", "Slovenia", "Solomon Islands", "Somalia", "Somoa", "South Africa", "South Korea", "Spain", "Sri Lanka", "Sudan", "Suriname", "Swaziland", "Sweden", "Switzerland", "Syria", "Taiwan", "Tajikistan", "Tanzania", "Thailand", "Timor-leste", "Togo", "Tonga", "Trinidad and Tobago", "Tunisia", "Turkey", "Turkmenistan", "Tuvalu", "UAE", "Uganda", "Ukraine", "United Kingdom", "United States", "Uruguay", "Uzbekistan", "Vanuatu", "Vatican City", "Venezuela", "Vietnam", "Western Sahara", "Yemen", "Zambia", "Zimbabwe"}
    Public quizImagesShuffled = quizImages.OrderBy(Function() randomNumberGenerator.Next).ToArray()
    Public quizScoreCounter As Integer
    Public quizOKCounter As Integer

    Private Sub btnQuestionMenu_Q01_Click(sender As System.Object, e As System.EventArgs) Handles btnQuestionMenu_Q01.Click
        frmQ0.Show()
        Me.Hide()
    End Sub

    Private Sub btnQuestionMenu_Q02_Click(sender As System.Object, e As System.EventArgs) Handles btnQuestionMenu_Q02.Click
        frmQ1.Show()
        Me.Hide()
    End Sub

    Private Sub btnQuestionMenu_Q03_Click(sender As System.Object, e As System.EventArgs) Handles btnQuestionMenu_Q03.Click
        frmQ2.Show()
        Me.Hide()
    End Sub

    Private Sub btnQuestionMenu_Q04_Click(sender As System.Object, e As System.EventArgs) Handles btnQuestionMenu_Q04.Click
        frmQ3.Show()
        Me.Hide()
    End Sub

    Private Sub btnQuestionMenu_Q05_Click(sender As System.Object, e As System.EventArgs) Handles btnQuestionMenu_Q05.Click
        frmQ4.Show()
        Me.Hide()
    End Sub

    Private Sub btnQuestionMenu_Q06_Click(sender As System.Object, e As System.EventArgs) Handles btnQuestionMenu_Q06.Click
        frmQ5.Show()
        Me.Hide()
    End Sub

    Private Sub btnQuestionMenu_Q07_Click(sender As System.Object, e As System.EventArgs) Handles btnQuestionMenu_Q07.Click
        frmQ6.Show()
        Me.Hide()
    End Sub

    Private Sub btnQuestionMenu_Q08_Click(sender As System.Object, e As System.EventArgs) Handles btnQuestionMenu_Q08.Click
        frmQ7.Show()
        Me.Hide()
    End Sub

    Private Sub btnQuestionMenu_Q09_Click(sender As System.Object, e As System.EventArgs) Handles btnQuestionMenu_Q09.Click
        frmQ8.Show()
        Me.Hide()
    End Sub

    Private Sub btnQuestionMenu_Q10_Click(sender As System.Object, e As System.EventArgs) Handles btnQuestionMenu_Q10.Click
        frmQ9.Show()
        Me.Hide()
    End Sub

    Private Sub btnQuestionMenu_Quit_Click(sender As System.Object, e As System.EventArgs) Handles btnQuestionMenu_Quit.Click
        closeQuestionForms()
        If Application.OpenForms().OfType(Of frmOptions).Any Then
            frmOptions.Close()
        End If
        Me.Close()
    End Sub

    Private Sub btnQuestionMenu_Finish_Click(sender As System.Object, e As System.EventArgs) Handles btnQuestionMenu_Finish.Click
        closeQuestionForms()
        frmScore.Show()
        Me.Close()
    End Sub

    Private Sub btnQuestionMenu_MainMenu_Click(sender As System.Object, e As System.EventArgs) Handles btnQuestionMenu_MainMenu.Click
        closeQuestionForms()
        frmWelcomeMenu.Show()
        Me.Close()
    End Sub
    Shared Sub closeQuestionForms()
        If Application.OpenForms().OfType(Of frmQ0).Any = True Then
            frmQ0.Close()
        End If
        If Application.OpenForms().OfType(Of frmQ1).Any = True Then
            frmQ1.Close()
        End If
        If Application.OpenForms().OfType(Of frmQ2).Any = True Then
            frmQ2.Close()
        End If
        If Application.OpenForms().OfType(Of frmQ3).Any = True Then
            frmQ3.Close()
        End If
        If Application.OpenForms().OfType(Of frmQ4).Any = True Then
            frmQ4.Close()
        End If
        If Application.OpenForms().OfType(Of frmQ5).Any = True Then
            frmQ5.Close()
        End If
        If Application.OpenForms().OfType(Of frmQ6).Any = True Then
            frmQ6.Close()
        End If
        If Application.OpenForms().OfType(Of frmQ7).Any = True Then
            frmQ7.Close()
        End If
        If Application.OpenForms().OfType(Of frmQ8).Any = True Then
            frmQ8.Close()
        End If
        If Application.OpenForms().OfType(Of frmQ9).Any = True Then
            frmQ9.Close()
        End If
    End Sub
End Class