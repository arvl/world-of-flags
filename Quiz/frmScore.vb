﻿Public Class frmScore

    Private Sub frmScore_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        Dim score As Integer = frmQuestionMenu.quizScoreCounter
        Dim mesg As String
        If score = 10 Then
            mesg = "N o i c e ."
        ElseIf score = 9 Or score = 8 Then
            mesg = "Good job"
        ElseIf score = 7 Or score = 6 Then
            mesg = "Not bad"
            lblScore_Congrats.Hide()
        ElseIf score = 5 Or score = 4 Then
            mesg = "Could be better"
            lblScore_Congrats.Hide()
        ElseIf score = 3 Or score = 2 Then
            mesg = "lol jk n00b :P"
        ElseIf score = 1 Then
            mesg = "b r a i n l e t"
            lblScore_questions.Text = "question"
        ElseIf score = 0 Then
            mesg = "b r a i n l e t"
        Else
            mesg = "Error 0x0511"
        End If
        If score.ToString.Length = 1 Then
            lblScore_realScore.Text = " " + score.ToString
        Else
            score.ToString()
            lblScore_realScore.Text = score.ToString
        End If
        lblScore_Msg.Text = mesg

        If frmOptions.quizSettingsAutoSaveScore = True Then
            Dim quizScoreFilepath As String = frmQuestionMenu.quizImagesPath
            Dim quizScoreFilename As String = "highscore.txt"
            If Not System.IO.File.Exists(quizScoreFilepath + quizScoreFilename) Then
                System.IO.File.Create(quizScoreFilepath + quizScoreFilename).Dispose()
            End If

            Dim quizScoreFileRead As New System.IO.StreamReader(quizScoreFilepath + quizScoreFilename)
            Dim score_old As Integer = Val(quizScoreFileRead.ReadToEnd)
            quizScoreFileRead.Close()

            If score_old < score Then
                Dim quizScoreFileWrite As New System.IO.StreamWriter(quizScoreFilepath + quizScoreFilename)
                quizScoreFileWrite.Write(score)
                quizScoreFileWrite.Close()
            End If
        End If
    End Sub

    Private Sub btnScore_Quit_Click(sender As System.Object, e As System.EventArgs) Handles btnScore_Quit.Click
        If Application.OpenForms().OfType(Of frmOptions).Any Then
            frmOptions.Close()
        End If
        Me.Close()
    End Sub

    Private Sub btnScore_Return_Click(sender As System.Object, e As System.EventArgs) Handles btnScore_Return.Click
        frmWelcomeMenu.Show()
        Me.Close()
    End Sub
End Class