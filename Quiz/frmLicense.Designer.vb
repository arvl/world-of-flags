﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmLicense
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmLicense))
        Me.rtxLicense_GPL = New System.Windows.Forms.RichTextBox()
        Me.SuspendLayout()
        '
        'rtxLicense_GPL
        '
        Me.rtxLicense_GPL.Font = New System.Drawing.Font("Lucida Console", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.rtxLicense_GPL.Location = New System.Drawing.Point(12, 12)
        Me.rtxLicense_GPL.Name = "rtxLicense_GPL"
        Me.rtxLicense_GPL.ReadOnly = True
        Me.rtxLicense_GPL.Size = New System.Drawing.Size(310, 258)
        Me.rtxLicense_GPL.TabIndex = 0
        Me.rtxLicense_GPL.Text = resources.GetString("rtxLicense_GPL.Text")
        '
        'frmLicense
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(334, 282)
        Me.Controls.Add(Me.rtxLicense_GPL)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.Name = "frmLicense"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "License"
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents rtxLicense_GPL As System.Windows.Forms.RichTextBox
End Class
