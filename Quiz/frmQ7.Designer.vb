﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmQ7
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.imgQ7_MainImage = New System.Windows.Forms.PictureBox()
        Me.btnQ7_OK = New System.Windows.Forms.Button()
        Me.rbtnQ7_Choice0 = New System.Windows.Forms.RadioButton()
        Me.rbtnQ7_Choice3 = New System.Windows.Forms.RadioButton()
        Me.rbtnQ7_Choice2 = New System.Windows.Forms.RadioButton()
        Me.rbtnQ7_Choice1 = New System.Windows.Forms.RadioButton()
        Me.lblQ7_Choice0 = New System.Windows.Forms.Label()
        Me.lblQ7_Choice1 = New System.Windows.Forms.Label()
        Me.lblQ7_Choice3 = New System.Windows.Forms.Label()
        Me.lblQ7_Choice2 = New System.Windows.Forms.Label()
        Me.btnQ7_Cancel = New System.Windows.Forms.Button()
        Me.lblQ7_MainQuestion = New System.Windows.Forms.Label()
        CType(Me.imgQ7_MainImage, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'imgQ7_MainImage
        '
        Me.imgQ7_MainImage.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom
        Me.imgQ7_MainImage.Location = New System.Drawing.Point(12, 12)
        Me.imgQ7_MainImage.Name = "imgQ7_MainImage"
        Me.imgQ7_MainImage.Size = New System.Drawing.Size(240, 180)
        Me.imgQ7_MainImage.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.imgQ7_MainImage.TabIndex = 0
        Me.imgQ7_MainImage.TabStop = False
        '
        'btnQ7_OK
        '
        Me.btnQ7_OK.Location = New System.Drawing.Point(192, 335)
        Me.btnQ7_OK.Name = "btnQ7_OK"
        Me.btnQ7_OK.Size = New System.Drawing.Size(60, 25)
        Me.btnQ7_OK.TabIndex = 1
        Me.btnQ7_OK.Text = "OK"
        Me.btnQ7_OK.UseVisualStyleBackColor = True
        '
        'rbtnQ7_Choice0
        '
        Me.rbtnQ7_Choice0.AutoSize = True
        Me.rbtnQ7_Choice0.Font = New System.Drawing.Font("Lucida Sans Typewriter", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.rbtnQ7_Choice0.ForeColor = System.Drawing.Color.RoyalBlue
        Me.rbtnQ7_Choice0.Location = New System.Drawing.Point(32, 219)
        Me.rbtnQ7_Choice0.Name = "rbtnQ7_Choice0"
        Me.rbtnQ7_Choice0.Size = New System.Drawing.Size(36, 22)
        Me.rbtnQ7_Choice0.TabIndex = 2
        Me.rbtnQ7_Choice0.TabStop = True
        Me.rbtnQ7_Choice0.Text = "A"
        Me.rbtnQ7_Choice0.UseVisualStyleBackColor = True
        '
        'rbtnQ7_Choice3
        '
        Me.rbtnQ7_Choice3.AccessibleDescription = ""
        Me.rbtnQ7_Choice3.AutoSize = True
        Me.rbtnQ7_Choice3.Font = New System.Drawing.Font("Lucida Sans Typewriter", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.rbtnQ7_Choice3.ForeColor = System.Drawing.Color.RoyalBlue
        Me.rbtnQ7_Choice3.Location = New System.Drawing.Point(32, 303)
        Me.rbtnQ7_Choice3.Name = "rbtnQ7_Choice3"
        Me.rbtnQ7_Choice3.Size = New System.Drawing.Size(36, 22)
        Me.rbtnQ7_Choice3.TabIndex = 3
        Me.rbtnQ7_Choice3.TabStop = True
        Me.rbtnQ7_Choice3.Text = "D"
        Me.rbtnQ7_Choice3.UseVisualStyleBackColor = True
        '
        'rbtnQ7_Choice2
        '
        Me.rbtnQ7_Choice2.AutoSize = True
        Me.rbtnQ7_Choice2.Font = New System.Drawing.Font("Lucida Sans Typewriter", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.rbtnQ7_Choice2.ForeColor = System.Drawing.Color.RoyalBlue
        Me.rbtnQ7_Choice2.Location = New System.Drawing.Point(32, 275)
        Me.rbtnQ7_Choice2.Name = "rbtnQ7_Choice2"
        Me.rbtnQ7_Choice2.Size = New System.Drawing.Size(36, 22)
        Me.rbtnQ7_Choice2.TabIndex = 4
        Me.rbtnQ7_Choice2.TabStop = True
        Me.rbtnQ7_Choice2.Text = "C"
        Me.rbtnQ7_Choice2.UseVisualStyleBackColor = True
        '
        'rbtnQ7_Choice1
        '
        Me.rbtnQ7_Choice1.AutoSize = True
        Me.rbtnQ7_Choice1.Font = New System.Drawing.Font("Lucida Sans Typewriter", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.rbtnQ7_Choice1.ForeColor = System.Drawing.Color.RoyalBlue
        Me.rbtnQ7_Choice1.Location = New System.Drawing.Point(32, 247)
        Me.rbtnQ7_Choice1.Name = "rbtnQ7_Choice1"
        Me.rbtnQ7_Choice1.Size = New System.Drawing.Size(36, 22)
        Me.rbtnQ7_Choice1.TabIndex = 5
        Me.rbtnQ7_Choice1.TabStop = True
        Me.rbtnQ7_Choice1.Text = "B"
        Me.rbtnQ7_Choice1.UseVisualStyleBackColor = True
        '
        'lblQ7_Choice0
        '
        Me.lblQ7_Choice0.AutoSize = True
        Me.lblQ7_Choice0.Location = New System.Drawing.Point(91, 224)
        Me.lblQ7_Choice0.Name = "lblQ7_Choice0"
        Me.lblQ7_Choice0.Size = New System.Drawing.Size(64, 13)
        Me.lblQ7_Choice0.TabIndex = 6
        Me.lblQ7_Choice0.Text = "<country A>"
        '
        'lblQ7_Choice1
        '
        Me.lblQ7_Choice1.AutoSize = True
        Me.lblQ7_Choice1.Location = New System.Drawing.Point(90, 252)
        Me.lblQ7_Choice1.Name = "lblQ7_Choice1"
        Me.lblQ7_Choice1.Size = New System.Drawing.Size(64, 13)
        Me.lblQ7_Choice1.TabIndex = 7
        Me.lblQ7_Choice1.Text = "<country B>"
        '
        'lblQ7_Choice3
        '
        Me.lblQ7_Choice3.AutoSize = True
        Me.lblQ7_Choice3.Location = New System.Drawing.Point(90, 308)
        Me.lblQ7_Choice3.Name = "lblQ7_Choice3"
        Me.lblQ7_Choice3.Size = New System.Drawing.Size(65, 13)
        Me.lblQ7_Choice3.TabIndex = 8
        Me.lblQ7_Choice3.Text = "<country D>"
        '
        'lblQ7_Choice2
        '
        Me.lblQ7_Choice2.AutoSize = True
        Me.lblQ7_Choice2.Location = New System.Drawing.Point(90, 279)
        Me.lblQ7_Choice2.Name = "lblQ7_Choice2"
        Me.lblQ7_Choice2.Size = New System.Drawing.Size(64, 13)
        Me.lblQ7_Choice2.TabIndex = 9
        Me.lblQ7_Choice2.Text = "<country C>"
        '
        'btnQ7_Cancel
        '
        Me.btnQ7_Cancel.Location = New System.Drawing.Point(126, 335)
        Me.btnQ7_Cancel.Name = "btnQ7_Cancel"
        Me.btnQ7_Cancel.Size = New System.Drawing.Size(60, 25)
        Me.btnQ7_Cancel.TabIndex = 10
        Me.btnQ7_Cancel.Text = "Cancel"
        Me.btnQ7_Cancel.UseVisualStyleBackColor = True
        '
        'lblQ7_MainQuestion
        '
        Me.lblQ7_MainQuestion.AutoSize = True
        Me.lblQ7_MainQuestion.Font = New System.Drawing.Font("Lucida Sans", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblQ7_MainQuestion.Location = New System.Drawing.Point(17, 198)
        Me.lblQ7_MainQuestion.Name = "lblQ7_MainQuestion"
        Me.lblQ7_MainQuestion.Size = New System.Drawing.Size(229, 15)
        Me.lblQ7_MainQuestion.TabIndex = 11
        Me.lblQ7_MainQuestion.Text = "Which country has this kind of flag?"
        '
        'frmQ7
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(264, 372)
        Me.ControlBox = False
        Me.Controls.Add(Me.lblQ7_MainQuestion)
        Me.Controls.Add(Me.btnQ7_Cancel)
        Me.Controls.Add(Me.lblQ7_Choice2)
        Me.Controls.Add(Me.lblQ7_Choice3)
        Me.Controls.Add(Me.lblQ7_Choice1)
        Me.Controls.Add(Me.lblQ7_Choice0)
        Me.Controls.Add(Me.rbtnQ7_Choice1)
        Me.Controls.Add(Me.rbtnQ7_Choice2)
        Me.Controls.Add(Me.rbtnQ7_Choice3)
        Me.Controls.Add(Me.rbtnQ7_Choice0)
        Me.Controls.Add(Me.btnQ7_OK)
        Me.Controls.Add(Me.imgQ7_MainImage)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.Name = "frmQ7"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Question 8"
        CType(Me.imgQ7_MainImage, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents imgQ7_MainImage As System.Windows.Forms.PictureBox
    Friend WithEvents btnQ7_OK As System.Windows.Forms.Button
    Friend WithEvents rbtnQ7_Choice0 As System.Windows.Forms.RadioButton
    Friend WithEvents rbtnQ7_Choice3 As System.Windows.Forms.RadioButton
    Friend WithEvents rbtnQ7_Choice2 As System.Windows.Forms.RadioButton
    Friend WithEvents rbtnQ7_Choice1 As System.Windows.Forms.RadioButton
    Friend WithEvents lblQ7_Choice0 As System.Windows.Forms.Label
    Friend WithEvents lblQ7_Choice1 As System.Windows.Forms.Label
    Friend WithEvents lblQ7_Choice3 As System.Windows.Forms.Label
    Friend WithEvents lblQ7_Choice2 As System.Windows.Forms.Label
    Friend WithEvents btnQ7_Cancel As System.Windows.Forms.Button
    Friend WithEvents lblQ7_MainQuestion As System.Windows.Forms.Label
End Class
